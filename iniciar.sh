#!/bin/sh

sudo xset s off
sudo xset -dpms
sudo xset s noblank

ARCHIVO='.temp-location'

YELLOW='\e[1;33m'
NC='\033[0m' # No Color
GREEN='\e[1;32m'
CYAN='\e[1;36m'

#Si el archivo existe
if [ -f ${ARCHIVO} ]; then
	#Leerlo y obtener la ruta al dispensador guardada prevamente
	printf "${CYAN}Leyendo archivo con ruta del dispensador...${NC} \n";
	while IFS='' read -r line || [ -n "$line" ]; do
	    PATHDISPENSADOR=$line;
	done < ${ARCHIVO}
	printf "${YELLOW}Borre el archivo ${ARCHIVO} si cambia la ruta del dispensador${NC} \n"
else
	#Encontrar la ruta al dispensador
	printf "${CYAN}Buscando dispensador...${NC} \n";
	PATHDISPENSADOR=$(find / -xdev 2>/dev/null -name "dispensador-de-cerveza" -type d);
	printf "${GREEN}Encontrado${NC} \n";
	echo ${PATHDISPENSADOR} > ${ARCHIVO}
fi

printf "${CYAN}Iniciando...${NC} \n";
cd ${PATHDISPENSADOR}/electron-dispensador
npm start
