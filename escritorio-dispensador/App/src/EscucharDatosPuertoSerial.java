/*
 * LeerPuertoSerial.java
 * 
 * Creado: 9/1/19
 * 
 * Autor: Jorge Rosas
 * 
 * */

public interface EscucharDatosPuertoSerial
{
	public void recibirDatosPuertoSerial ( String datos );
}
