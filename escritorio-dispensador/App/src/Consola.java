/*
 * Consola.java
 * 
 * Creado: 8/1/19
 * 
 * Autor: Jorge Rosas
 * 
 * Descripci�n: Clase puramente de prueba para enviar informaci�n al puerto y ver qu� responde
 * */

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.DefaultCaret;

public class Consola extends JFrame implements EscucharDatosPuertoSerial
{
	private static final long serialVersionUID = 1L;
	
	private PuertoSerial ps;
	private JTextArea consola;
	private JTextField mensaje;
	
	public Consola ()
	{
		setTitle( "Test" );
		setSize( new Dimension( 500, 490 ) );
		setDefaultCloseOperation( EXIT_ON_CLOSE );
		setLocationRelativeTo( null );
		setLayout( null );

		consola = new JTextArea();
		consola.setFont( new Font( "console", Font.PLAIN, 14 ) );
		( ( DefaultCaret ) consola.getCaret() ).setUpdatePolicy( DefaultCaret.ALWAYS_UPDATE );
		
		JScrollPane scroll = new JScrollPane( consola );
		scroll.setBounds( 5, 5, 470, 400 );
		add( scroll );
		
		mensaje = new JTextField();
		mensaje.addKeyListener( new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				if ( e.getKeyChar() == '\n' )
					enviarMensaje();
			}
			
			@Override
			public void keyReleased(KeyEvent e) {}
			
			@Override
			public void keyPressed(KeyEvent e) {}
		} );
		mensaje.setBounds( 5, 410, 395, 25 );
		add( mensaje );
		
		JButton enviar = new JButton( "Enviar" );
		enviar.setBounds( 405, 410, 70, 25 );
		add( enviar );
		enviar.addActionListener( new ActionListener() {
			
			@Override
			public void actionPerformed ( ActionEvent e )
			{
				enviarMensaje();
			}
		} );
		
		ps = new PuertoSerial( "COM3", this );
		
		setVisible( true );
	}
	
	private void enviarMensaje ()
	{
		String msj = mensaje.getText();
		ps.enviarMensaje( msj + "\n" );
		agregarMensaje( " >> " + msj);
		mensaje.setText( "" );
	}
	
	private void agregarMensaje ( String mensaje )
	{
		if ( consola != null )
			consola.append( mensaje + "\n" );
		else
			System.err.println( "Consola no iniciada, mensaje: " + mensaje );
	}
	
	long tiempo = 0;
	
	@Override
	public void recibirDatosPuertoSerial ( String datos )
	{
		agregarMensaje( " << " + datos );
		
		if ( datos.contains( "2" ) )
		{
			long nuevo = System.currentTimeMillis();
			agregarMensaje( " | | " + ( nuevo - tiempo ) );
			tiempo = nuevo;
			mensaje.setText( "2" );
			enviarMensaje();
		} else if ( datos.contains( "1" ) )
		{
			mensaje.setText( "1" );
			enviarMensaje();
		} else if ( datos.contains( "0" ) )
		{
			mensaje.setText( "0" );
			enviarMensaje();
		}
	}
}
