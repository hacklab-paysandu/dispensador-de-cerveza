/*
 * PuertoSerial.java
 * 
 * Creado: 8/1/19
 * 
 * Autor: Jorge Rosas
 * 
 * */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

public class PuertoSerial implements SerialPortEventListener
{
	// Tiempo de desconexi�n si no hay respuesta
	private static final int TIEMPO = 2000;

	private SerialPort puertoSerial;
	private BufferedReader entrada;
	private OutputStream salida;
	private ArrayList<EscucharDatosPuertoSerial> escuchasDatos;

	// Distintos constructores ( todos llaman al de abajo )
	public PuertoSerial ( String puerto ) { this( puerto, 9600, null ); }
	public PuertoSerial ( String puerto, EscucharDatosPuertoSerial escucharDatosPuertoSerial ) { this( puerto, 9600, escucharDatosPuertoSerial ); }
	public PuertoSerial ( String puerto, int baud ) { this( puerto, baud, null ); }

	// Constructor
	public PuertoSerial ( String puerto, int baud, EscucharDatosPuertoSerial escucharDatosPuertoSerial )
	{
		// Se agrega el escucha de datos que se pase
		agregarEscuchaDatosPuertoSerial( escucharDatosPuertoSerial );
		
		CommPortIdentifier puertoId = null;
		Enumeration<?> enumCom;
		
		// Se obtienen los puertos que contengan algo conectado en el equipo
		enumCom = CommPortIdentifier.getPortIdentifiers();
		
		// Mientras queden puertos por revisar
		while ( enumCom.hasMoreElements() )
		{
			// Se toma el siguiente puerto
			puertoId = ( CommPortIdentifier ) enumCom.nextElement();
			
			// Se busca un puerto serial con el nombre indicado en la variable puerto ( ej. COM3 )
			if ( puertoId.getPortType() == CommPortIdentifier.PORT_SERIAL && puertoId.getName().contains( puerto ) )
			{
				System.out.println( "Puerto encontrado: " + puertoId.getName() );
				break;
			}
		}
		
		// Si el puerto es null o no se encontro el indicado se sale
		// TODO: quiz� cambiar este procedimiento de entontrar el puerto...
		// el constructor recorre todos los puertos que tenga, y se conecta con todos los seriales
		// luego le manda un mensaje y si el puerto responde correctamente entonces es el arduino que buscamos...
		if ( puertoId == null || !puertoId.getName().contains( puerto ) )
		{
			System.err.println( "No se pudo conectar al puerto " + puerto );
			System.exit( 1 );
		}
		
		try
		{
			// Se crea la conexi�n con el puerto serial, hay que indicarle un nombre para la conexi�n y un tiempo de desconexi�n en caso de no recibir respuesta
			puertoSerial = ( SerialPort ) puertoId.open( this.getClass().getName(), TIEMPO );
			puertoSerial.setSerialPortParams(
					baud, // 9600 por defecto
					SerialPort.DATABITS_8,
					SerialPort.STOPBITS_1,
					SerialPort.PARITY_NONE
			);
			
			// La variable entrada va a ser donde quede almacenada la informaci�n que recibimos del puerto
			entrada = new BufferedReader( new InputStreamReader( puertoSerial.getInputStream() ) );
			// La variable salida va a ser utilizada para enviar informaci�n al puerto
			salida = puertoSerial.getOutputStream();
			
			// Hay que indicarle al puerto con qu� clase se debe comunicar cuando recibe un dato ( ver funcion 'serialEvent' )
			puertoSerial.addEventListener( this );
			puertoSerial.notifyOnDataAvailable( true );
		} catch ( Exception e )
		{
			System.err.println( e.toString() );
		}
	}
	
	/* Funci�n: agregarEscuchaDatosPuertoSerial
	 * 
	 * Params:
	 * 		- escucharDatosPuertoSerial: Interfaz con la que se comunicar� al recibir informaci�n por el puerto
	 * 
	 * Descripci�n: Hay una lista de escuchas, cuando se le pasa uno a esta funci�n queda guardado en dicha
	 * lista para que a la hora de recibir informaci�n, se la pueda delegar a todos, esto permite tener
	 * muchas clases y procesos distintos escuchando el mismo puerto.
	 * */
	public void agregarEscuchaDatosPuertoSerial ( EscucharDatosPuertoSerial escucharDatosPuertoSerial )
	{
		if ( this.escuchasDatos == null )
			this.escuchasDatos = new ArrayList<EscucharDatosPuertoSerial>();
		
		if ( escucharDatosPuertoSerial != null )
			this.escuchasDatos.add( escucharDatosPuertoSerial );
	}
	
	/* Funci�n: enviarMensaje
	 * 
	 * Params:
	 * 		- mensaje: Informaci�n a escribir en el puerto
	 * */
	public void enviarMensaje ( String mensaje )
	{
		try
		{
			salida.write( mensaje.getBytes() );
		} catch ( IOException e )
		{
			System.err.println( e.toString() );
		}
	}

	/* Funci�n: serialEvent
	 * 
	 * Params:
	 * 		- spe: Evento recibido del puerto
	 * 
	 * Descripci�n: Esta es la funcion de la interfáz 'SerialPortEventListener', cuando suceda algo en el puerto
	 * lo recibiremos aqu�
	 * */
	@Override
	public void serialEvent ( SerialPortEvent spe )
	{
		// Si el tipo de evento es que hay informaci�n disponible para ser leida
		if ( spe.getEventType() == SerialPortEvent.DATA_AVAILABLE )
		{
			try
			{
				// Se manda a todos los escuchas
				for ( EscucharDatosPuertoSerial e : escuchasDatos )
					e.recibirDatosPuertoSerial( entrada.readLine() );
			} catch ( Exception e )
			{
				System.err.println( e.toString() );
			}
		}
	}

	/* Funci�n: cerrar
	 *
	 * Descripci�n: Remueve los escuchas y cierra el puerto para liberarlo
	 * */
	public void cerrar ()
	{
		if ( puertoSerial != null )
		{
			puertoSerial.removeEventListener();
			puertoSerial.close();
		}
	}
}
