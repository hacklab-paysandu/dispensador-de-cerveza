//Escribir RFID
#include <SPI.h>
#include <MFRC522.h>

#define SS_PIN    10
#define RST_PIN   9


#define B_USUARIO 4
#define B_ADMIN   5

MFRC522 rfid( SS_PIN, RST_PIN );
MFRC522::MIFARE_Key key;


void setup() {
  Serial.begin(9600);
  Serial.println( "Escriba:" );
  Serial.println( "'bloque:valor' sin las comillas" );
  Serial.println( " Las opciones para bloque son: 'admin'" );
  Serial.println( "                               'usuario'" );
  Serial.println();  
  Serial.println( " Las opciones para valor son: 'admin'" );
  Serial.println( "                               cualquier numero que sera interpretado como saldo" );

  //Inicializar RFID
  SPI.begin();
  rfid.PCD_Init();

  // Llave de la tarjeta por defecto para la tarjeta RFID
  for ( byte i = 0; i < 6; i++ ) key.keyByte[i] = 0xFF;
  
}

void loop() {
    // Chequear por tarjetas RFID
    if ( !rfid.PICC_IsNewCardPresent() )
      return; // Pasa si encuentra tarjeta nueva
  
    if ( !rfid.PICC_ReadCardSerial() )
      return; // Pasa si puede leer el serial de la tarjeta
      
      Serial.println();
      Serial.println( "=================================== Tarjeta detectada ===================================" );
      Serial.println();

      byte lecturaAdmin[16];
      leerBloque( B_ADMIN, lecturaAdmin );
      Serial.print( "Bloque admin: " );
      Serial.println( obtenerString( lecturaAdmin, 16 ) );

      byte lecturaUsu[16];
      leerBloque( B_USUARIO, lecturaUsu );
      Serial.print( "Bloque usuario: " );
      Serial.println( obtenerString( lecturaUsu, 16 ) );

      Serial.println();

      rfid.PCD_StopCrypto1(); // Magia ...

    while ( prepararTarjeta() ) {
      if ( Serial.available() > 0 ) {
          String input = Serial.readString();
          int sepIndex = input.indexOf( ':' ); // Índice del separador
          String dato = input.substring( 0, sepIndex ); // Dato a la izquierda del ':'
          String valor = input.substring( sepIndex + 1, input.length() - 1);

          byte contenido[16];
          String( valor ).getBytes( contenido, 16 );

          if ( dato == "admin" ){
            if ( escribirBloque(B_ADMIN, contenido) == 0 ){
              Serial.println( "Escritura realizada correctamente en bloque admin" );
              if ( leerBloque( B_ADMIN, lecturaAdmin ) == 0 ){
                Serial.print( "Bloque admin: " );
                Serial.println(obtenerString( lecturaAdmin, 16 ));
              }
              rfid.PCD_StopCrypto1(); // Magia
            }
          } else if ( dato == "usuario" ) {
             if ( escribirBloque(B_USUARIO, contenido) == 0 ){
              Serial.println( "Escritura realizada correctamente en bloque usuario" );
              if ( leerBloque( B_USUARIO, lecturaUsu ) == 0 ){
                  Serial.print( "Bloque usuario: " );
                  Serial.println(obtenerString( lecturaUsu, 16 ));
              }
              rfid.PCD_StopCrypto1(); // Magia
            }
          } else {
            Serial.println( "Bloque incorrecto" );
            Serial.println();   
          }
      }
   }

    // Se desconecta internamente
    rfid.PICC_HaltA();
    rfid.PCD_StopCrypto1();

    Serial.println();
    Serial.println( "=================================== Tarjeta removida ====================================" );

    
 
}

int intentos = 0;
const int MAX_INTENTOS = 10;
bool prepararTarjeta (){
  intentos = 0;
  
  // Mientras no detecte tarjeta y mientras no pueda leer el serial, que siga
  while ( true ){
    if ( rfid.PICC_IsNewCardPresent() && rfid.PICC_ReadCardSerial() )
      return true;

    intentos++;
    if ( intentos >= MAX_INTENTOS ) // Si se pasa de intentos, que de la tarjeta como retirada
      return false;
  }
}

int escribirBloque ( byte bloque, byte arreglo[] ){
  int largestModulo4Number = bloque / 4 * 4;
  int trailerBloque = largestModulo4Number + 3;

  if ( bloque > 2 && ( bloque + 1 ) % 4 == 0 ){
    Serial.print( bloque );
    Serial.println( " es un trailerBlock" );
    return 2;
  }

  MFRC522::StatusCode estado = rfid.PCD_Authenticate( MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBloque, &key, &( rfid.uid ) );

  if ( estado != MFRC522::STATUS_OK ){
    Serial.print( "PCD_Authenticate() Write fallo:" );
    Serial.println( rfid.GetStatusCodeName( estado ) );
    return 3;
  }

  estado = rfid.MIFARE_Write( bloque, arreglo, 16 );

  if ( estado != MFRC522::STATUS_OK ){
    Serial.print( "MIFARE_Write() fallo:" );
    Serial.println( rfid.GetStatusCodeName( estado ) );
    return 4;
  }

  return 0;
}

int leerBloque ( byte bloque, byte arreglo[] ){
  int largestModulo4Number = bloque / 4 * 4;
  int trailerBloque = largestModulo4Number + 3;

  MFRC522::StatusCode estado = rfid.PCD_Authenticate( MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBloque, &key, &( rfid.uid ) );

  if ( estado != MFRC522::STATUS_OK ){
    Serial.print( "PCD_Authenticate() Read fallo:" );
    Serial.println( rfid.GetStatusCodeName( estado ) );
    return 3;
  }

  byte tamBuffer = 18;

  estado = rfid.MIFARE_Read( bloque, arreglo, &tamBuffer );

  if ( estado != MFRC522::STATUS_OK ){
    Serial.print( "MIFARE_Read() fallo:" );
    Serial.println( rfid.GetStatusCodeName( estado ) );
    return 4;
  }

  return 0;
}

String obtenerString ( byte arreglo[], int tam )
{
  String res = "";

  for ( int i = 0; i < tam - 2; i++ )
    res += ( char ) arreglo[i];

  return res;
}
