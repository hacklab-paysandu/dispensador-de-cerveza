package com.example.jorge.dispensadorapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jorge.dispensadorapp.BD.BDCliente;
import com.example.jorge.dispensadorapp.BD.BDInterface;
import com.example.jorge.dispensadorapp.BD.RespuestaUsuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Principal extends AppCompatActivity {

    private static final String TAG = "TAG_Principal";

    private TextView tvNombre;
    private TextView tvCorreo;
    private TextView tvSaldo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        tvNombre = ( TextView ) findViewById( R.id.tvNombre );
        tvCorreo = ( TextView ) findViewById( R.id.tvCorreo );
        tvSaldo = ( TextView ) findViewById( R.id.tvSaldo );

        BDInterface bd = BDCliente.getClient().create( BDInterface.class );
        Call<RespuestaUsuario> call = bd.consultarUsuario( "rosasjorge1701@gmail.com" );
        call.enqueue(new Callback<RespuestaUsuario>() {
            @Override
            public void onResponse(Call<RespuestaUsuario> call, Response<RespuestaUsuario> response) {
                tvNombre.setText( response.body().getNombre() );
                tvCorreo.setText( response.body().getCorreo() );
                tvSaldo.setText( "Saldo: $ " + response.body().getSaldo() );

                Log.v( TAG, response.body().getNombre() );
            }

            @Override
            public void onFailure(Call<RespuestaUsuario> call, Throwable t) {
                Log.v( TAG, "Error!!! " + t.getMessage() );
            }
        });
    }

    public void QR ( View v ) {
        Intent i = new Intent( Principal.this, CodigoQR.class );
        i.putExtra( "texto", "hola" );
        startActivity( i );
    }
    
    public void CerrarSesion ( View v ) {
        Toast.makeText(this, "Cerrar sesion", Toast.LENGTH_SHORT).show();
    }
}
