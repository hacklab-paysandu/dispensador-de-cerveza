package com.example.jorge.dispensadorapp.BD;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface BDInterface {

    @GET( "crear" )
    Call<Respuesta> crear (
            @Query( "nombre" ) String nombre,
            @Query( "correo" ) String correo,
            @Query( "pass" ) String pass
    );

    @FormUrlEncoded
    @POST( "consultarUsuario" )
    Call<RespuestaUsuario> consultarUsuario (
            @Field( "correo" ) String correo
    );
}
