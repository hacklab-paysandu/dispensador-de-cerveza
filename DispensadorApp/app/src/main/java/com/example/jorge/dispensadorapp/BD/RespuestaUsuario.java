package com.example.jorge.dispensadorapp.BD;

import com.google.gson.annotations.SerializedName;

public class RespuestaUsuario extends Respuesta {

    @SerializedName( "nombre" )
    private String nombre;
    @SerializedName( "correo" )
    private String correo;
    @SerializedName( "saldo" )
    private String saldo;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }
}
