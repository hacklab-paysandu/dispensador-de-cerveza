package com.example.jorge.dispensadorapp;

import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.zxing.WriterException;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class CodigoQR extends AppCompatActivity {

    private static final String TAG = "TAG_CodigoQR";

    ImageView codigoQR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_codigo_qr);

        codigoQR = ( ImageView ) findViewById( R.id.codigoQR );

        // Obtener el tamaño de la pantalla para generar la imagen
        WindowManager wm = ( WindowManager ) getSystemService( WINDOW_SERVICE );
        Display d = wm.getDefaultDisplay();
        Point punto = new Point();
        d.getSize( punto );

        int ancho = punto.x;
        int alto = punto.y;

        int dimension = ancho < alto ? ancho : alto;

        // Obtener texto para el código QR
        String texto = getIntent().getStringExtra( "texto" );

        // Generar QR
        QRGEncoder qrg = new QRGEncoder(
                texto,
                null,
                QRGContents.Type.TEXT,
                dimension
        );

        try {
            // Mostrar QR
            codigoQR.setImageBitmap( qrg.encodeAsBitmap() );
        } catch ( WriterException e ) {
            Log.v( TAG, e.toString() );
        }
    }
}
