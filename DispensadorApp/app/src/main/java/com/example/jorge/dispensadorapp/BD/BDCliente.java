package com.example.jorge.dispensadorapp.BD;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BDCliente {

    public static final String URL = "http://192.168.20.142:3000/api/";
    private static Retrofit retrofit = null;

    public static Retrofit getClient () {
        if ( retrofit == null ) {
            retrofit = new Retrofit.Builder()
                    .baseUrl( URL )
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }
}
