/* Creado: 2-1-2019
 * Autor: Jorge Rosas
 * Version: 1.0
 * */

#include <SPI.h>
#include <MFRC522.h>

#define PIN_RST   9
#define PIN_SS    10

#define BLOQUE    4

MFRC522 rfid( PIN_SS, PIN_RST );
MFRC522::MIFARE_Key key;

void setup ()
{
  Serial.begin( 9600 );
  SPI.begin();
  rfid.PCD_Init();

  // Llave de la tarjeta por defecto
  for ( byte i = 0; i < 6; i++ ) key.keyByte[i] = 0xFF;
}

uint8_t control = 0x00;
int intentos = 0;
const int MAX_INTENTOS = 100;

void loop ()
{
  if ( !rfid.PICC_IsNewCardPresent() ){
    return; // Pasa si detecta nueva tarjeta 
  }

  if ( !rfid.PICC_ReadCardSerial() ){
    return; // Pasa si puede leer el serial de la tarjeta
  }
  
  // Se envia el uid al pc
  Serial.print( "uid:" );
  Serial.print( id( rfid.uid.uidByte, rfid.uid.size ) ); // Se obtiene el id
  Serial.println();

  // Se envia el saldo al pc
  Serial.print( "saldo:" );
  Serial.println( leer( BLOQUE ).toFloat() );
  
  while ( true )
  {
    // Controlar que la tarjeta siga ahí
    control = 0;

    for ( int i = 0; i < 3; i++ )
    {
      if ( !rfid.PICC_IsNewCardPresent() )
      {
        if ( rfid.PICC_ReadCardSerial() )
          control |= 0x16;
          
        if ( rfid.PICC_ReadCardSerial() )
          control |= 0x16;

        control += 0x1;
      }

      control += 0x4;
    } // Termina control

    if ( control == 13 || control == 14 ) // Tarjeta sigue ahí
    {
      if ( Serial.available() )
      {
        String info = Serial.readString();

        intentos = 0;
        // Mientras no detecte tarjeta y mientras no pueda leer el serial, que siga
        while ( !rfid.PICC_IsNewCardPresent() || !rfid.PICC_ReadCardSerial() )
        {
          intentos++;
          if ( intentos >= MAX_INTENTOS ) // Si se pasa de intentos, que de la tarjeta como retirada
          {
            Serial.println( "error:Muchos intentos para conectar con la tarjeta" );
            desconectar();
            return;
          }
        }
        
        if ( info == "0\n" ) // Si recibe 0, se manda el saldo actual
        {
          Serial.print( "saldo:" );
          Serial.println( leer( BLOQUE ).toFloat() );
        }
        else // Cualquier otra cosa, se escribe en el bloque 4
        {
          escribir( info, BLOQUE );
        }
      }
    }
    else
      break; // Tarjeta removida
  }
  
  desconectar();
}

void desconectar ()
{
  Serial.println( "no-tarjeta" );

  rfid.PICC_HaltA();
  rfid.PCD_StopCrypto1();
}

String leer ( byte bloque )
{
  byte info[18];
  byte len = 18;

  // Se autentica con la tarjeta
  MFRC522::StatusCode estado = ( MFRC522::StatusCode ) rfid.PCD_Authenticate( MFRC522::PICC_CMD_MF_AUTH_KEY_A, 7, &key, &( rfid.uid ) );

  if ( estado != MFRC522::STATUS_OK )
  {
    desconectar();
    return "";
  }
    
  // Lee el contenido del bloque especificado
  estado = rfid.MIFARE_Read( bloque, info, &len );
  if ( estado != MFRC522::STATUS_OK )
  {
    desconectar();
    return "";
  }

  // Se recorre y se guarda en un String que posteriormente se retorna
  String res = "";
  
  for ( uint8_t i = 0; i < 16; i++ )
    if ( info[i] != 32 )
      res += ( char ) info[i];

  rfid.PCD_StopCrypto1();
  return res;
}

// Escribe el dato en el bloque
void escribir ( String dato, byte bloque )
{
  byte tam = 34;
  byte info[tam];

  // Se pasa el dato String a byte[]
  dato.getBytes( info, tam );
  
  // Se autentica con la tarjeta
  MFRC522::StatusCode estado = ( MFRC522::StatusCode ) rfid.PCD_Authenticate( MFRC522::PICC_CMD_MF_AUTH_KEY_A, 7, &key, &( rfid.uid ) );

  if ( estado != MFRC522::STATUS_OK )
  {
    desconectar();
    return;
  }
    
  // Se escribe
  estado = rfid.MIFARE_Write( bloque, info, 16 );
  if ( estado != MFRC522::STATUS_OK ) // Se chequea si fallo o no
  {
    desconectar();
    return;
  }
}

// Convierte la id del buffer en un String
String id ( byte *buffer, byte bufferSize )
{
  String res = "";

  for ( byte i = 0; i < bufferSize; i++ )
    res += buffer[i];

  return res;
}
