const fs = require( 'fs' );
const SerialPort = require( 'serialport' );
const Readline = require( '@serialport/parser-readline' );
const ipcRenderer = require('electron').ipcRenderer;

let puerto = null;
let parser = null;

const reintentarConectarCada = 5000; // ms
let reintentoConexion = 0;

//Nombres de las cervezas.
let nombresCervezas = [];
//ID de la tarjeta
let id;
//Cantidad de cervezas
let cantidad = 0;


/* guardarDatosArchivo
 * 
 * Params:
 * 		- id: id del usuario que realizó la acción
 * 		- accion: acción realizada
 * 		- info: información extra sobre la acción
 *		- archivo: nombre del archivo donde guardar los datos.
 * 
 * Función para guardar información de lo que pasa en le dispensador
 * */
guardarDatosArchivo = ( id, accion, info, archivo ) => {

	// Se toma el tiempo actual
	let date = new Date();
	date.setTime( new Date().getTime() - 10800000 );

	// Se le da formato a la fecha: dia/mes/año - hora:minutos:segundos
	let fecha = date.getUTCDate() + '/' + ( date.getUTCMonth() + 1 ) + '/' + date.getUTCFullYear() + ' - ' + date.getUTCHours() + ':' + date.getUTCMinutes() + ':' + date.getUTCSeconds();

	// Se le da formato al mensaje separado por coma: 	[id del usuario],[accion realizada],[informacion extra],[fecha]
	let log = id.toString().trim() + ',' + accion + ',' + info + ',' + fecha;
	log = log.replace( '\n', '' );

	// Se agrega al final del archivo
	fs.appendFile( archivo, log + '\n', ( err ) => {
		if ( err )
			console.log( 'Error guardando log: ' + err );
	} );
};

editarConfig = ( clave, valor ) => {
	let linea = clave + ":" + valor + '\n';
	try {
		fs.appendFileSync( 'config.cfg', linea);
	} catch ( err ) {		
		console.log( 'Error guardando datos: ' + err);
	}
	
}

// Agregar el id del div de las ventanas aquí
const ventanas = [ inicio, ingresar, cervezas, admin ];
/* mostrarVentana
 * 
 * Deja como hidden a todos los divs que representan ventanas en la aplicación
 * y deja visible el div correspondiente a la ventana que se quiere mostrar
 * */

 let ventanaAnterior = '';

mostrarVentana = ( ventana ) => {
	
	ventanas.map( v => {
		if ( v !== cervezas ) //Se ocultan todas las ventanas menos 'cervezas'.
			v.style.visibility = 'hidden';
		else if ( ventanaAnterior !== cervezas ) //Si la ventana es cervezas, pero no es la anterior se oculta igual.
			v.style.visibility = 'hidden';
	});

	// Casos especiales:
	if ( ventana === cervezas ) {
		saldo.innerHTML = '_._';
		for ( let i = 1; i <= 4; i++ ) {
			document.getElementById('c' + i).innerHTML = '0.0';
			document.getElementById('t' + i).innerHTML = '0.0';
		}
		
		progreso.style.transform = 'translateY(0%)';
		btnTerminar.style.visibility = 'unset';
		msjRetire.style.visibility = 'hidden';

	}else if ( ventana == inicio) {
		inicio.getElementsByClassName('cargando')[0].style.visibility = 'unset';
		datos.style.visibility = 'hidden';
	}

	//Si la ventana anterior es cervezas, primero se hace la animacion y despues se muestra la ventana elegida, sino se muestra directamente.
	if ( ventanaAnterior === cervezas ) {
		var cdiv = ventanaAnterior.getElementsByClassName('cervezas')[0];
		var sdiv = ventanaAnterior.getElementsByClassName('saldo')[0];
		
		//Agregar clases con animacion de salida.
		cdiv.classList.add('cervezas-animation-out');
		sdiv.classList.add('saldo-animation-out');
		
		//Se ejecuta luego de terminada la transicion.
		transitionOutFinish = () => {
			cdiv.classList.remove( 'cervezas-animation' );
			sdiv.classList.remove( 'saldo-animation' );
			cdiv.classList.remove( 'cervezas-animation-out' );
			sdiv.classList.remove( 'saldo-animation-out' );
			
			cervezas.style.visibility = 'hidden';
			ventana.style.visibility = 'visible';

			cdiv.removeEventListener( 'transitionend', transitionOutFinish );
		}

		cdiv.addEventListener( 'transitionend', transitionOutFinish);

	} else {
		ventana.style.visibility = 'visible';
	}

	//Agregar clases con animacion de entrada.
	if ( ventana == cervezas ) {
		cervezas.getElementsByClassName('cervezas')[0].classList.add('cervezas-animation');
		cervezas.getElementsByClassName('saldo')[0].classList.add('saldo-animation');
	} 

	//Limpiar texto de inicio
	if ( ventana !== inicio )
		inicioTexto.innerHTML = 'Cargando...';

	//Actualizar la ventana anterior.
	ventanaAnterior = ventana;
}

let usuarioAdmin = false;
let saldoInicial = 0;

let conectado = false;
let recibido = false;
let reenviar = false;


/* parserData
 *
 * Función que recibirá la información del puerto
 *
 * conectado: Arduino se acaba de conectad y esta esperando una respuesta con datos iniciales
 * no-tarjeta: Se retiró la tarjeta del módulo
 * tarjeta: Se colocó una tarjeta en el módulo
 * saldo:cantidad: Nos indica el saldo actual de la tarjeta colocada
 * cauda1: Caudal de cantidad de cerveza 1 consumida
 * cauda2: Caudal de cantidad de cerveza 2 consumida
 * monto1: Monto a cobrar por la cantidad de cerveza 1 consumida
 * monto2: Monto a cobrar por la cantidad de cerveza 2 consumida
 * */
parserData = ( data ) => {
	console.log(data);
	if ( conectado ) // Si ya se tiene conexión con Arduino
	{
		if ( data.includes( 'tarjeta-admin' ) )
		{
			usuarioAdmin = true;
			mostrarVentana( admin );

			// Se leen los logs
			fs.readFile( 'log.csv', ( err, contenido ) => {
				if ( err )
					return error('Error leyendo log de datos', 'parserData', err);

				// Se borra todo de la tabla
				while ( tablaAdmin.hasChildNodes() )
					tablaAdmin.removeChild( tablaAdmin.lastChild );

				// Se separa el contenido del archivo de logs en lineas
				contenido.toString().split( '\n' ).map( p => {
					// Por cada línea se crea un tr ( Table Row, fila de columna )
					let tr = document.createElement( 'tr' );

					// Se separa el contenido de cada línea por comas
					let partes = p.split( ',' ).map( p2 => {
						// Se crea un td por cada dato separado por coma y se lo agrega al tr ( Table Data, dato de la tabla )
						let td = document.createElement( 'td' );
						td.innerText = p2;
						tr.appendChild( td );
					} );

					// Se agrega cada uno de los tr a la tabla
					tablaAdmin.appendChild( tr );
				} );
			} );
		}
		else if ( data.includes( 'no-tarjeta' ) )
		{
			if ( !usuarioAdmin )
			{
				// Log

				//Se obtienen los totales de las compras de cada cerveza.
				let total1 = +t1.innerHTML;
				let total2 = +t2.innerHTML;
				let total3 = +t3.innerHTML;
				let total4 = +t4.innerHTML;

				if ( total1 > 0 )
					guardarDatosArchivo( id, nombresCervezas[0], total1.toString(), 'log.csv' );

				if ( total2 > 0 )
					guardarDatosArchivo( id, nombresCervezas[1], total2.toString(), 'log.csv' );

				if ( total3 > 0 )
					guardarDatosArchivo( id, nombresCervezas[2], total3.toString(), 'log.csv' );

				if ( total4 > 0 )
					guardarDatosArchivo( id, nombresCervezas[3], total4.toString(), 'log.csv' );

				if ( total1 > 0  || total2 > 0 || total3 > 0 || total4 > 0 )
					guardarDatosArchivo( id, 'TOTAL', total1 + total2 + total3 + total4, 'log.csv' );
			
				
				guardarDatosArchivo(id,'Última vez', saldo.innerHTML.toString(), 'datos-compra.csv');
				id = '';
			}

			// Se vuelve a la pantalla de espera
			mostrarVentana( ingresar );
		}
		else if ( data.includes( 'tarjeta' ) )
		{
			usuarioAdmin = false;

			//Obtener el id de la tarjeta y guardarla.
			let info = data.split( ':' );
			id = info[1];

			// Se muestra la pantalla de cobro
			mostrarVentana( cervezas );
			
			// Se le pide el saldo de la tarjeta al Arduino
			puerto.write( 'saldo:pedir' );
		}
		else if ( data.includes( 'saldoi' ) )
		{
			saldoInicial = Number( +data.split( ':' )[1] ).toFixed( 1 );
			saldo.innerHTML = saldoInicial;
		}
		else if ( data.includes( 'saldo' ) )
		{
			// Se recibió el saldo y se muestra en pantalla
			let nuevoSaldo = Number( +data.split( ':' )[1] ).toFixed( 1 );
			saldo.innerHTML = nuevoSaldo;
			let translateY =  100 - ( ( nuevoSaldo * 100 ) / saldoInicial );
			progreso.style.transform = 'translateY('+ translateY + '%)';
		}
		else if ( data.includes( 'cauda' ) )
		{
			let info = data.split( ':' );
			let caudal = document.getElementById( 'c' + info[1] );

			console.log(info[1],info[2]);

			caudal.innerText = Number( +info[2] ).toFixed( 1 );
		}
		else if ( data.includes( 'monto' ) )
		{
			let info = data.split( ':' );
			let total = document.getElementById( 't' + info[1] );

			total.innerText = Number( +info[2] ).toFixed( 1 );
		}
	}
	else // Mensajes para establecer la conexión con Arduino
	{

		if ( data.includes( 'conectado' ) )
		{
			// Arduino constantemente envía 'conectado', hasta que le responden

			if ( !recibido ) {
				setTimeout( () => {
					puerto.write( 'c' );
				}, 2000);

				if ( reenviar ) {
					inicioTexto.innerHTML = 'Conexión establecida<br>Actualizando datos';	
				} else {
					inicioTexto.innerHTML = 'Conexión establecida<br>Enviando datos iniciales';
				}

				inicio.getElementsByClassName('cargando')[0].style.visibility = 'hidden';
				datos.style.visibility = 'unset';

				enviarDatosProgreso( 0, config.length - 1 );
			}

			recibido = true;

		}
		else if ( data.includes( 'recibir' ) )
		{

			reenviar = false;
			
			// Arduino indica que esta listo para recibir la información
			// Se le envía el primer dato
			puerto.write( config[posEnviado] );
			enviarDatosProgreso( posEnviado + 1, config.length - 1 );
		}
		else if ( data.includes( 'fin:fin' ) )
		{
			// Arduino envía un 'fin:fin' como último mensaje
			// Este debe estar al final de la lista de en el archivo config.cfg
			conectado = true
			mostrarVentana( ingresar );
			posEnviado = 0;
		}
		else if ( data.includes( config[posEnviado] ) ) // Cuando Arduino nos responde con un dato que ya enviamos
		{
			// Se envía el siguiente
			posEnviado++;
			puerto.write( config[posEnviado] );
			enviarDatosProgreso( posEnviado + 1, config.length - 1 );
		}
	}
}

//Se llama cuando se dispara el evento al desconectar el arduino.
reconectar = () => {
	console.log('Arduino desconectado, puerto cerrado.');
	reintentoConexion = 0;
	reintentarConectar();
}

/* conectar
 * 
 * Establece la conexión con la placa Arduino
 * */
conectar = () => {
	mostrarVentana( inicio );

	//Si hubo una conexion anterior
	if ( puerto != null ) {
			conectado = false;
			recibido = false;
		//Si el puerto estaba abierto, cerrarlo antes de continuar.	
		if ( puerto.isOpen ) {
			puerto.removeListener('close',reconectar);	
			puerto.close( (err) => {
				if (err)
					return console.log('No se pudo cerrar el puerto', err);

				puerto = null;
				continuarConexion();
			} );
		} else {
			puerto = null;
			continuarConexion();
		}	
	} else {
		continuarConexion();
	}
	
}

continuarConexion = () => {

	SerialPort.list( ( err, puertos ) => {
		// Si no hay puertos e intenta de nuevo
		if ( puertos.length <= 1 ) {
			console.log('No hay puertos');
			reintentarConectar();
		} else {
			//Se recorre la lista de puertos encontrados
			puertos.forEach((p) => {

				//Si en el puerto está conectado un Arduino y todavia no hay abierto ningún puerto
				let manufacturer = p['manufacturer'];
				if ( manufacturer != undefined ) {

					console.log('puerto', p);

					if ( puerto == null && ( manufacturer.toLowerCase().includes('arduino') || manufacturer.toLowerCase().includes('1a86')) ){
						inicioTexto.innerHTML = 'Puerto de comunicación detectado ' + p.comName + '<br>Estableciendo conexión...';

						// Se crea el puerto
						puerto = new SerialPort( p.comName, { autoOpen: false, baudRate: 9600 } );
						// Se abre el puerto
						puerto.open( ( err ) => {
							if ( err ) {
								// Si falla la conexión se coloca el puerto en null y se intenta de nuevo
								error('Problema estableciendo comunicación', 'continuarConexion', err);
								reintentarConectar();
							} else {
								// Se comunica al usuario
								inicioTexto.innerHTML = 'Conexión establecida';
								inicioReintento.innerHTML = '';

								// Se crea el parser ( objero que va a interpretar los datos )
								parser = new Readline();
								// Se setea la función a la que el parser va a enviar los datos
								parser.on( 'data', parserData );
								// Se le dice al puerto se utilice el parser
								puerto.pipe( parser );

								//Si se desconecta el arduino volver a reintentar la conexion.
								puerto.on('close', reconectar);
							}
						});
					}
				}
			});

			//Si luego de chequear todos los puertos no se encontró un Arduino, reintentar la conexión
			if (puerto == null) {
				 reintentarConectar();
			}
		}
	} );
}



/* reintentarConectar
 * 
 * Avisa al usuario y reintenta la conexión luego de 'reintentarConectarCada' milesimas
 * */
 let timeoutReintentarConectar = null;
reintentarConectar = () => {	
	console.log('Reintentando conectar');

	mostrarVentana( inicio );

	// Se acumula cada reintento.
	reintentoConexion++;

	let reintentarConectarCadaSegundos = (reintentarConectarCada / 1000);

	//Se muestra un mensaje con 'reintentarConectarCadaSegundos' segundos.
	inicioTexto.innerHTML = 'No se detectó Arduino conectado<br>Reintentando en <p style="display:inline;" >' + reintentarConectarCadaSegundos + '</p>';
	
	let inicioTextoP = inicioTexto.getElementsByTagName('p')[0];

	//Se crean el resto de mensajes en timeouts para que de efecto de cuenta regresiva.
	let seg = reintentarConectarCadaSegundos - 1;
	for (let i = 1; i <= reintentarConectarCadaSegundos - 1; i++ ) {
		setTimeout( function( s ) { inicioTextoP.innerHTML = s; }, (1000 * i), seg );
		seg--;
	}

	//Se muestra la cantidad de reintentos realizados.
	inicioReintento.innerHTML = ( reintentoConexion != 0 ? ' (' + reintentoConexion + ') ' : '' );

	// Se setea el reintento en el tiempo dado.
	timeoutReintentarConectar = setTimeout( () => {
		conectar();
	}, reintentarConectarCada );
}

let config = [];
let posEnviado = 0;

/* cargarConfiguracion
 * 
 * Carga la configuración del sistema y llama a la función para conectarse a Arduino
 * */
cargarConfiguracion = () => {

	mostrarVentana(inicio);
	posEnviado = 0;

	// Se lee el archivo que guarda la configuración, la variable 'contenido' es un String que contiene todas las lineas del archivo
	// El archivo de configuración por cada línea tiene el siguiente formato: variable, dos puntos y el valor de la variable
	// variable:valor

	fs.readFile( './config.cfg', ( err, contenido ) => {
		if ( err )
			return error('Problema al leer archivo de configuración', 'cargarConfiguracion', err);

		// Se guarda cada línea del archivo por separado en el arregro 'config'
		config = contenido.toString().split( '\n' );

		// Se recorren todas las lineas
		for ( let i = 0; i < config.length; i++ )
		{
			// Se separa la información de cada linea en un arreglo
			let info = config[i].split( ':' );

			//Guardar datos en variables para su posterior uso.
			if ( info[0].includes( 'nombre' ) )
				nombresCervezas.push( info[1] );

			// Se muestran los paneles para la cantidad deseada de cervezas en el dispensador
			if ( info[0].includes( 'cantidad' ) ) {
				cantidad = info[1];

				var clase = "cerveza" + (cantidad.toString()).trim();

				// Se ocultan todas las cervezas y se agrega la clase correspondiente a la cantidad.
				for ( let i = 1; i <= 4; i++ ){
					var c = document.getElementById( 'cerveza' + i );
					c.style.visibility = 'hidden';
					c.classList.add(clase);
				}

				// Se hacen visibles la cantidad de cervezas indicadas.
				for ( let i = 1; i <= cantidad; i++ )
					document.getElementById( 'cerveza' + i ).style.visibility = 'unset';
			}

			// Se intenta obtener un elemento con el nombre de la variable
			let elementos = document.getElementsByClassName( info[0] );

			for ( elemento of elementos ) {
				// Si existe el elemento se le coloca el valor
				if ( elemento !== undefined && elemento !== null ) {
					//Si es un input, se trata del menu del administrador.
					if ( elemento.nodeName == "INPUT" ) {
						elemento.value = info[1];
						elemento.parentElement.parentElement.removeAttribute("hidden");
					} else {
						elemento.innerHTML = info[1];

					}
				}
			}

		}


		// Conectar al Arduino
		conectar();

		//Simular funcionamiento del sistema
		//simular();

	} );
}

simular = ()=>{

	if(timeoutReintentarConectar !== null)
		clearTimeout(timeoutReintentarConectar);

	mostrarVentana(ingresar);

	setTimeout(() => {
		
		mostrarVentana(cervezas);
		saldo.innerHTML = '100';
		
		setTimeout(()=>{

			simularservida = () => {

				let translateY = parseFloat( progreso.style.transform.toString().split('(')[1].split('%')[0] );
				let s = parseFloat(saldo.innerHTML);
				let precio1 = 1000 / parseFloat(cervezas.getElementsByClassName("precio1")[0].innerHTML);

				if ( translateY <  100 ) {
					progreso.style.transform = 'translateY('+ ( translateY + 1 )+ '%)';
					saldo.innerHTML = s - 1;
					t1.innerHTML = + parseFloat(t1.innerHTML) + 1;
					c1.innerHTML = (parseFloat(c1.innerHTML) + precio1).toFixed(1);
					setTimeout(() => {simularservida()}, 70);
			    } else {
			    	setTimeout(() => { btnTerminar.click(); }, 2000);
			    	setTimeout(() => { mostrarVentana ( ingresar ) }, 5000);
			    	setTimeout(() => { mostrarVentana( admin ) }, 8000);
			    	setTimeout(() => { mostrarVentana ( ingresar ) }, 12000);
			    }
			}
			simularservida();
		}, 2000);
	}, 3000);

	console.log("Simulando");
}

btnEditar.onclick = () => {
	let inputs = tablaEditar.getElementsByTagName('input');
	let posicion = 0;

	try {
		fs.writeFileSync('config.cfg', '');
	} catch(err) {
		error('Ha ocurrido un problema al editar el archivo de configuración', 'btnEditar.onclick', err);
	}

	for ( var i = 0; i < inputs.length; i = i + 2 ) {
		if ( inputs[i].parentElement.parentElement.hidden != true ) {
			posicion++;
			editarConfig ( 'nombre' + posicion, inputs[i].value );
		    editarConfig ( 'precio' + posicion, inputs[i + 1].value );
		}
	}
	editarConfig ( 'cantidad', posicion );
	editarConfig ( 'fin', 'fin' );

	reenviar = true;

	cargarConfiguracion();
}

btnTerminar.onclick = () => {
	if ( puerto !== null )
		puerto.write('retirar');
	btnTerminar.style.visibility = 'hidden';
	msjRetire.style.visibility = 'visible';
}

btnApagar.onclick = () => {
	//Notificar al main.js para mostrar confirmación de apagado.
	ipcRenderer.send( 'request-mainprocess-action', {orden: 'apagar'} );
}

error = (mensaje ,functionName, error) => {
	console.log(functionName, error);
	inicioTexto.innerHTML = mensaje;
	mostrarVentana(inicio);
	inicio.getElementsByClassName('cargando')[0].style.visibility = 'hidden';
}

enviarDatosProgreso = (posicion, total) => {
	progresoDatos.style.width = ( ( posicion / total ) * 100 ) +  '%';
	datos.getElementsByTagName('span')[0].innerText = posicion + '/' + total;
};

// Punto de entrada al script
cargarConfiguracion();

acciones = ( event ) => {
	var control = event.ctrlKey;
	if (control){
		console.log("true");
		switch( event.key ) {
			case 'c':
					cargarConfiguracion();
			break;
			case 's':
					simular();
			break;
		}
	}
}



//Deshabilitar advertencias
window['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';

//Registrar al precionar teclas.
window.addEventListener('keyup', acciones, true);


/*let inputs = document.getElementsByTagName('input');
for (i of inputs){
	console.log('input', i);
	i.onclick = () => {
		ipcRenderer.send( 'request-mainprocess-action', {orden: 'teclado'} );
		console.log('orden enviada');
	};
}*/
