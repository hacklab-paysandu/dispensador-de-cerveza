const { app, BrowserWindow, dialog, ipcMain } = require( 'electron' );
let shell;

let ventana;

crearVentana = () => {
	// Crea la venata
	ventana = new BrowserWindow({
		show:false,
  		webPreferences: { nodeIntegration: true }
	});
	
	// Quita la barra del menú
	ventana.setMenu( null );

	// Abre el panel de desarrollo
	//ventana.webContents.openDevTools();

	// Ventana completa
	ventana.setFullScreen( true );

	// Carga la página index.html
	ventana.loadFile( 'index.html' );

	//Registrar eventos al precionar teclas
	ventana.webContents.on('before-input-event', (event, input) => {
		if(input.type === 'keyUp')
			switch(input.key){
				case 'F12':
					//Abrir y cerrar herramientas de desarrollo.
					ventana.webContents.toggleDevTools();
				break;
				case 'F11':
					//Cambiar a pantalla completa.
					if(!ventana.isFullScreen())
						ventana.setFullScreen(true);
					else
						ventana.setFullScreen(false);
				break;
				case 'Escape':
					//Cerrar aplicacion
					ventana.destroy();
				break;
				case 'r':
					if(input.control){
						//Reiniciar
						app.relaunch();
						ventana.destroy();
					}
				break;
			}	
	});

	//Esperar orden del render.js
	ipcMain.on('request-mainprocess-action', (event, arg) => {
		switch( arg.orden ){
			case 'apagar': //Si la orden es apagar
				let opciones = {
			 			buttons: ["Si","No"],
			 			message: "¿ Está seguro que desea apagar el dispensador ?"
				}

				//Mostrar dialogo de confirmación.
				dialog.showMessageBox(ventana, opciones, (respuesta) => {
					if (respuesta === 0)
						apagar();
				});
			break;
		}
	} );

	ventana.on('ready-to-show', () => {
			console.log('App lista');
			ventana.show();
	});

	ventana.on( 'closed', () => {
		ventana = null;
	} );
};

//app.disableHardwareAcceleration();

// Cuando la app esta lista crea una ventana
app.on( 'ready', () => {
	crearVentana();
});

// La aplicación se cierra cuando todas las ventanas fueron cerradas
app.on( 'window-all-closed', () => {
	// En macOS las aplicaciones suelen quedar activas hasta que el usuario presiona Cmd + Q
	if ( process.platform !== 'darwin' ) // darwin == macOS
		app.quit();
} );

app.on( 'activate', () => {
	// En macOS se acostumbra a re-crear una ventana cuando el usuario
	// presiona el "dock icon"
	if ( ventana === null )
		crearVentana();
} );

// Apagar dispensador
apagar = () => {

	if ( shell === undefined )
		shell = require( 'shelljs' );

	shell.exec( 'shutdown now', { async: true, silent: false }, ( data, output ) => {console.log( data, output )});
}

process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';