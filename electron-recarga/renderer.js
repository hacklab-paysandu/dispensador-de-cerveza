const fs = require( 'fs' );
const SerialPort = require( 'serialport' );
const Readline = require( '@serialport/parser-readline' );

let puerto = null;
let parser = null;

const reintentarConectarCada = 5000; // ms
let reintentoConexion = 0;
let saldoInicial = 0;

conectar = () =>
{
	mostrarVentana( inicio );

	if(puerto != null){
			conectado = false;
			recibido = false;
		if(puerto.isOpen){

			puerto.removeListener('close',reconectar);

			puerto.close((err) => {
				if(err)
					return console.log('No se pudo cerrar el puerto', err);

				puerto = null;
				continuarConexion();
			});
		}else{
			puerto = null;
			continuarConexion();
		}	
	}else{
		continuarConexion();
	}
	
}


continuarConexion = () =>
{
	SerialPort.list( ( err, puertos ) => {
		// Si no hay puertos e intenta de nuevo
		if ( puertos.length === 0 )
		{
			console.log('No hay puertos');
			reintentarConectar();
		}
		else
		{

			//Se recorre la lista de puertos encontrados
			puertos.forEach((p) => {

				//Si en el puerto está conectado un Arduino y todavia no hay abierto ningún puerto
				let manufacturer = p['manufacturer'];
				if ( manufacturer != undefined){
					console.log('puerto',p);
					if (puerto == null && (manufacturer.includes('wch.cn') || manufacturer.includes('1a86'))) {
						inicioTexto.innerHTML = 'Puerto de comunicación detectado ' + p.comName + '<br>Estableciendo conexión...';

						// Se crea el puerto
						puerto = new SerialPort( p.comName, { autoOpen: false, baudRate: 9600 } );
						// Se abre el puerto
						puerto.open( ( err ) => {
							if ( err )
							{
								// Si falla la conexión se coloca el puerto en null y se intenta de nuevo
								inicioTexto.innerHTML = 'Error al establecer la conexión: ' + err;
								reintentarConectar();
							}
							else
							{
								// Se comunica al usuario
								inicioTexto.innerHTML = 'Conexión establecida';
								inicioReintento.innerHTML = '';

								// Se crea el parser ( objero que va a interpretar los datos )
								parser = new Readline();
								// Se setea la función a la que el parser va a enviar los datos
								parser.on( 'data', parserData );
								// Se le dice al puerto se utilice el parser
								puerto.pipe( parser );

								reconectar = () => {
									console.log('Arduino desconectado, puerto cerrado.');
									reintentoConexion = 0;
									reintentarConectar()
								}

								puerto.on( 'close', reconectar );

								mostrarVentana(ingresar);
							}
						});
					}
				}
			});

			//Si luego de chequear todos los puertos no se encontró un Arduino, reintentar la conexión
			if (puerto == null)
         		 reintentarConectar();
		}
	} );
}

/* reintentarConectar
 * 
 * Avisa al usuario y reintenta la conexión luego de 'reintentarConectarCada' milesimas
 * */
reintentarConectar = () =>
{
	console.log('Reintentando conectar');

	mostrarVentana( inicio );

	// Se acumula cada reintento
	reintentoConexion++;
	
	let reintentarConectarCadaSegundos = (reintentarConectarCada / 1000);

	//Se muestra un mensaje con 'reintentarConectarCadaSegundos' segundos.
	inicioTexto.innerHTML = 'No se detectó Arduino conectado<br>Reintentando en ' + reintentarConectarCadaSegundos;
	
	//Se crean el resto de mensajes en timeouts para que de efecto de cuenta regresiva.
	let seg = reintentarConectarCadaSegundos - 1;
	for (let i = 1; i <= reintentarConectarCadaSegundos - 1; i++ ) {
		setTimeout( function( s ) { inicioTexto.innerHTML = 'No se detectó Arduino conectado<br>Reintentando en ' + ( s ); }, (1000 * i), seg );
		seg--;
	}

	//Se muestra la cantidad de reintentos realizados.
	inicioReintento.innerHTML = ( reintentoConexion != 0 ? ' (' + reintentoConexion + ') ' : '' );
	
	// Se setea el reintento en el tiempo dado
	setTimeout( () => {
		conectar();
	}, reintentarConectarCada );
}



// Agregar el id del div de las ventanas aquí
const ventanas = [inicio, ingresar, cargar];

mostrarVentana = ( ventana ) =>
{
	// Se esconden todas las ventanas
	ventanas.map( v => v.style.display = 'none' );

	// Se muestra la ventana elegida
	ventana.style.display = 'block';

	if ( ventana === cargar ){
		saldo.innerHTML = '_._';
		cantidad.value = '';
		cantidad.focus();
	}
}


/* Aquí llegan los datos del puerto
 * 
 * no-tarjeta: Se retiró la tarjeta del módulo
 * tarjeta: Se colocó una tarjeta en el módulo
 * saldo:cantidad: Nos indica el saldo actual de la tarjeta colocada
 * */
parserData = ( data ) =>
{
	if ( data.includes( 'no-tarjeta' ) )
	{
		guardarDatosArchivo(id.innerHTML, 'Última vez', saldo.innerHTML, 'datos-compra.csv');
		mostrarVentana( ingresar );
	}
	else if ( data.includes( 'saldo' ) )
	{
		mostrarVentana( cargar );
		saldoInicial = Number( +data.split( ':' )[1] ).toFixed( 1 );
		saldo.innerHTML = '$' + saldoInicial;
	}
	else if ( data.includes( 'uid' ) )
	{
		mostrarVentana( cargar );
		id.innerHTML = data.split( ':' )[1];
	}
}

/* Al hacer click en el boton cargar se envía un número al puerto
 * El programa que se está ejecutando en Arduino suma ese número al monto actual
 * */
recargar = () => {
	puerto.write( ( +saldoInicial + +cantidad.value.trim() ) + '' );
	cantidad.value = '';
}

btnCargar.onclick = () => {
	recargar()
};

document.onkeypress = (e) => {
    e = e || window.event;
    if(e.which == 13)
    	recargar();
};

guardarDatosArchivo = ( id, accion, info, archivo ) => {

	// Se toma el tiempo actual
	let date = new Date();
	date.setTime( new Date().getTime() - 10800000 );

	// Se le da formato a la fecha: dia/mes/año - hora:minutos:segundos
	let fecha = date.getUTCDate() + '/' + ( date.getUTCMonth() + 1 ) + '/' + date.getUTCFullYear() + ' - ' + date.getUTCHours() + ':' + date.getUTCMinutes() + ':' + date.getUTCSeconds();

	// Se le da formato al mensaje separado por coma: 	[id del usuario],[accion realizada],[informacion extra],[fecha]
	let log = id + ',' + accion + ',' + info + ',' + fecha;
	log = log.replace( '\n', '' );

	// Se agrega al final del archivo
	fs.appendFile( archivo, log + '\n', ( err ) => {
		if ( err )
			console.log( 'Error guardando log: ' + err );
	} );
};

conectar();

acciones = (event) => {
	switch(event.key){
		case 'c':
			if(event.ctrlKey === true)
				conectar();
		break;
	}
}

//Deshabilitar advertencias
window['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';

//Registrar al precionar teclas.
window.addEventListener('keyup', acciones, true);