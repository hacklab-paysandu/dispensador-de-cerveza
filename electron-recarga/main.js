const { app, BrowserWindow } = require( 'electron' );

let ventana;

crearVentana = () => {
	// Crea la venata
	ventana = new BrowserWindow({
		show:false,
  		webPreferences: { nodeIntegration: true },
  	});

	// Quita la barra del menú
	ventana.setMenu( null );

	// Abre el panel de desarrollo
	//ventana.webContents.openDevTools();
	// Ventana completa
	//ventana.setFullScreen( true );

	// Carga la página index.html
	ventana.loadFile( 'index.html' );
	
	//Registrar eventos al precionar teclas
	ventana.webContents.on('before-input-event', (event, input) => {
		if(input.type === 'keyUp')
			switch(input.key){
				case 'F12':
					//Abrir y cerrar herramientas de desarrollo.
					ventana.webContents.toggleDevTools();
				break;
				case 'F11':
					//Cambiar a pantalla completa.
					if(!ventana.isFullScreen())
						ventana.setFullScreen(true);
					else
						ventana.setFullScreen(false);
				break;
				case 'Escape':
					//Cerrar aplicacion
					ventana.destroy();
				break;
				case 'r':
					if(input.control){
						//Reiniciar
						app.relaunch();
						ventana.destroy();
					}
				break;
			}	
	});

	ventana.once('ready-to-show', () => {
			console.log('App lista');
			//ventana.maximize();
			ventana.show();
	});

	ventana.on( 'closed', () => {
		ventana = null;
	} );

	
};

process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';

// Cuando la app esta lista crea una ventana
app.on( 'ready', () => {
	crearVentana();
});

// La aplicación se cierra cuando todas las ventanas fueron cerradas
app.on( 'window-all-closed', () => {
	// En macOS las aplicaciones suelen quedar activas hasta que el usuario presiona Cmd + Q
	if ( process.platform !== 'darwin' ) // darwin == macOS
		app.quit();
} );

app.on( 'activate', () => {
	// En macOS se acostumbra a re-crear una ventana cuando el usuario
	// presiona el "dock icon"
	if ( ventana === null )
		crearVentana();
} );