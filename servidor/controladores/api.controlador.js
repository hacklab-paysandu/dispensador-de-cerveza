var mongoose = require( 'mongoose' );
const Usuario = require( '../modelos/usuario.modelo' );

exports.crear = ( req, res ) => {
	let nombre = req.query.nombre;
	let correo = req.query.correo;
	let pass = req.query.pass;

	if ( nombre === undefined || nombre === '' )
		return res.json( { estado: 'ERR', mensaje: 'Falta nombre' } );

	if ( correo === undefined || correo === '' )
		return res.json( { estado: 'ERR', mensaje: 'Falta correo' } );

	if ( pass === undefined || pass === '' )
		return res.json( { estado: 'ERR', mensaje: 'Falta pass' } );

	Usuario.find( { correo: correo } )
	.then( ( usuarios ) => {
		if ( usuarios.length === 0 ) {
			new Usuario( {
				_id: mongoose.Types.ObjectId(),
				nombre: nombre,
				correo: correo,
				pass: pass
			} ).save()
			.then( ( u ) => {
				res.json( { estado: 'OK', mensaje: 'Usuario creado' } );
			} )
			.catch( ( err ) => {
				res.json( { estado: 'ERR', mensaje: 'Error: ' + err } );
			} );
		} else {
			res.json( { estado: 'ERR', mensaje: 'Correo ya existe' } );
		}
	} )
	.catch( ( err ) => {
		res.json( { estado: 'ERR', mensaje: 'Error: ' + err } );
	} );
};

exports.iniciarSesion = ( req, res ) => {
	let correo = req.query.correo;
	let pass = req.query.pass;

	if ( correo === undefined || correo === '' )
		return res.json( { estado: 'ERR', mensaje: 'Falta correo' } );

	if ( pass === undefined || pass === '' )
		return res.json( { estado: 'ERR', mensaje: 'Falta pass' } );

	Usuario.findOne( { correo: correo } )
	.then( ( usuario ) => {
		if ( !usuario )
			return res.json( { estado: 'ERR', mensaje: 'El correo no está registrado' } );

		if ( usuario.pass != pass )
			return res.json( { estado: 'ERR', mensaje: 'Contraseña incorrecta' } );

		res.json( { estado: 'OK', mensaje: 'Datos correctos' } );
	} )
	.catch( ( err ) => {
		res.json( { estado: 'ERR', mensaje: 'Error: ' + err } );
	} );
};

exports.cargarSaldo = ( req, res ) => {
	let correo = req.query.correo;
	let cantidad = req.query.cantidad;

	if ( correo === undefined || correo === '' )
		return res.json( { estado: 'ERR', mensaje: 'Falta correo' } );

	if ( cantidad === undefined || cantidad === '' )
		return res.json( { estado: 'ERR', mensaje: 'Falta cantidad' } );

	Usuario.findOneAndUpdate( { correo: correo }, { $inc: { saldo: cantidad } }, ( err, usuario ) => {
		if ( err )
			return res.json( { estado: 'ERR', mensaje: 'Error: ' + err } );

		if ( !usuario )
			return res.json( { estado: 'ERR', mensaje: 'El correo no está registrado' } );

		res.json( { estado: 'OK', mensaje: { saldo: +usuario.saldo + +cantidad } } );
	} );
};

exports.consultarUsuario = ( req, res ) => {
	let correo = req.body.correo;

	console.log( 'Correo: ' + correo );

	if ( correo === undefined || correo === '' )
		return res.json( { estado: 'ERR', mensaje: 'Falta correo' } );

	Usuario.findOne( { correo: correo }, ( err, usuario ) => {
		if ( err )
			return res.json( { estado: 'ERR', mensaje: 'Error: ' + err } );

		if ( !usuario )
			return res.json( { estado: 'ERR', mensaje: 'El correo no está registrado' } );

		res.json( { estado: 'OK', mensaje: 'OK', correo: usuario.correo, nombre: usuario.nombre, saldo: usuario.saldo } );
	} );
};