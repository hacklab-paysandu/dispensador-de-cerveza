const express = require( 'express' );
const bodyParser = require( 'body-parser' );
const mongoose = require( 'mongoose' );

// Conectar con base de datos
var mongoDB = 'mongodb://servidor_user:servidor_pass1@ds161856.mlab.com:61856/servidor_bd';

mongoose.connect( mongoDB, { useNewUrlParser: true } );
mongoose.Promise = global.Promise;

var db = mongoose.connection;
db.on( 'error', console.error.bind( console, 'MongoDB error de conección' ) );

// Crear servidor
const app = express();
app.use( bodyParser.json() );
app.use( '/api', require( './rutas/api.ruta' ) );

let puerto = 3000;

const servidor = app.listen( process.env.PORT || puerto, () => {
	console.log( 'Servidor iniciado' );
} );