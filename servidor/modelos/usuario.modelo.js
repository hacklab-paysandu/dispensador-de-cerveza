var mongoose = require( 'mongoose' );
var Schema = mongoose.Schema;

var usuarioSchema = new Schema( {
	_id: mongoose.Schema.Types.ObjectId,
	nombre: {
		type: String,
		required: true,
		max: 50
	},
	correo: {
		type: String,
		required: true,
		unique: true
	},
	pass: {
		type: String,
		required: true
	},
	saldo: {
		type: Number,
		default: 0
	}
} );

module.exports = mongoose.model( 'Usuario', usuarioSchema );