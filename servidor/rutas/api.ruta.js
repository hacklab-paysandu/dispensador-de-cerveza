const express = require( 'express' );
const router = express.Router();

const api_controlador = require( '../controladores/api.controlador' );

router.get( '/crear', api_controlador.crear );
router.get( '/iniciarSesion', api_controlador.iniciarSesion );
router.get( '/cargarSaldo', api_controlador.cargarSaldo );
router.post( '/consultarUsuario', api_controlador.consultarUsuario );

module.exports = router;