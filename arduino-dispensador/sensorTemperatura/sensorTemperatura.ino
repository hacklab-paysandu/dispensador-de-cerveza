/* Creado: 2/1/2019
 * Autor: Jorge Rosas
 * Version: 0.1
 * 
 * https://create.arduino.cc/projecthub/TheGadgetBoy/ds18b20-digital-temperature-sensor-and-arduino-9cc806
 * */
#include <OneWire.h>
#include <DallasTemperature.h>

#define PIN_SENSOR_TEMPERATURA  4
#define PIN_SENSOR_TEMPERATURA2  5
OneWire oneWire( PIN_SENSOR_TEMPERATURA );
OneWire oneWire2( PIN_SENSOR_TEMPERATURA2 );
DallasTemperature sensores( &oneWire );
DallasTemperature sensores2( &oneWire2 );

void setup ()
{
  Serial.begin(9600);
  sensores.begin();
  sensores2.begin();
}

void loop ()
{
  Serial.readString();
  sensores.requestTemperatures();
  Serial.print( "Temperatura1: " ); 
  Serial.print( sensores.getTempCByIndex(0) ); // ByIndex, porque se puede tener mas de un sensor
  sensores2.requestTemperatures();
  Serial.print( "        Temperatura2: " ); 
  Serial.println( sensores2.getTempCByIndex(0) ); // ByIndex, porque se puede tener mas de un sensor
  //delay( 100 );
}
