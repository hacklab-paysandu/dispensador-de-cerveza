int intentos = 0;
const int MAX_INTENTOS = 10;

bool prepararTarjeta ()
{
  intentos = 0;
  
  // Mientras no detecte tarjeta y mientras no pueda leer el serial, que siga
  while ( true )
  {
    if ( rfid.PICC_IsNewCardPresent() && rfid.PICC_ReadCardSerial() )
      return true;

    intentos++;
    if ( intentos >= MAX_INTENTOS ) // Si se pasa de intentos, que de la tarjeta como retirada
      return false;
  }
}

int escribirBloque ( byte bloque, byte arreglo[] )
{
  int largestModulo4Number = bloque / 4 * 4;
  int trailerBloque = largestModulo4Number + 3;

  if ( bloque > 2 && ( bloque + 1 ) % 4 == 0 )
  {
    Serial.print( bloque );
    Serial.println( " es un trailerBlock" );
    return 2;
  }

  MFRC522::StatusCode estado = rfid.PCD_Authenticate( MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBloque, &key, &( rfid.uid ) );

  if ( estado != MFRC522::STATUS_OK )
  {
    Serial.print( "PCD_Authenticate() Write fallo:" );
    Serial.println( rfid.GetStatusCodeName( estado ) );
    return 3;
  }

  estado = rfid.MIFARE_Write( bloque, arreglo, 16 );

  if ( estado != MFRC522::STATUS_OK )
  {
    Serial.print( "MIFARE_Write() fallo:" );
    Serial.println( rfid.GetStatusCodeName( estado ) );
    return 4;
  }
}

int leerBloque ( byte bloque, byte arreglo[] )
{
  int largestModulo4Number = bloque / 4 * 4;
  int trailerBloque = largestModulo4Number + 3;

  MFRC522::StatusCode estado = rfid.PCD_Authenticate( MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBloque, &key, &( rfid.uid ) );

  if ( estado != MFRC522::STATUS_OK )
  {
    Serial.print( "PCD_Authenticate() Read fallo:" );
    Serial.println( rfid.GetStatusCodeName( estado ) );
    return 3;
  }

  byte tamBuffer = 18;

  estado = rfid.MIFARE_Read( bloque, arreglo, &tamBuffer );

  if ( estado != MFRC522::STATUS_OK )
  {
    Serial.print( "MIFARE_Read() fallo:" );
    Serial.println( rfid.GetStatusCodeName( estado ) );
    return 4;
  }
}

String obtenerString ( byte arreglo[], int tam )
{
  String res = "";

  for ( int i = 0; i < tam - 2; i++ )
    res += ( char ) arreglo[i];

  return res;
}
