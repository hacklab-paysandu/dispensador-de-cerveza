#include <SPI.h>
#include <MFRC522.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Servo.h>
#include <stdlib.h>

// Variables rfid
#define SS_PIN    9
#define RST_PIN   8

#define BLOQUE    4
#define B_ADMIN   5

MFRC522 rfid( SS_PIN, RST_PIN );
MFRC522::MIFARE_Key key;

byte contenido[16];
byte lectura[18];
// ----------------------
// Variables conexion
bool conectado = false;
bool recibido = false;
// ----------------------
// Variables caudalimetro
int pasosCaudal[] = { 0, 0, 0, 0 };
float totalCaudal[] = { 0, 0, 0, 0 };
float caudalAMl = 6;
// ----------------------
// Sensores de temperatura
#define PIN_SENSOR_TEMPERATURA    4

OneWire oneWire( PIN_SENSOR_TEMPERATURA );
DallasTemperature sensores( &oneWire );

int medirTempCada = 5000;
int medirTemp = medirTempCada;
// ----------------------
// Servo
#define PIN_SERVO_TARJETA         5

#define SERVO_ARRIBA              60
#define SERVO_ABAJO               90

#define LED_VERDE                 11
#define LED_ROJO                  12

Servo tarjeta;
// ----------------------
// Variables funcionalidad
int CANTCERVEZAS = 1;

// Pin del relee de las electrovalvulas
#define EV  6

// Pines de los caudalimetros, pines posibles en arduino mega ( 2, 3, 18, 19, 20, 21)
int CAUDA[] = { 2, 3, 20, 21 };

String admin;

float saldoInicial = 0;
float nuevoSaldo = 0;

double precio[] = { 0.1, 0.1, 0.1, 0.1 };
double monto[] = { 0, 0, 0, 0 };

long antes; // Medir el tiempo que pasa

bool puedeContar = false;
// ----------------------

void setup ()
{
  Serial.begin( 9600 );

  // Inicializar RFID
  SPI.begin();
  rfid.PCD_Init();

  // Llave de la tarjeta por defecto para la tarjeta RFID
  for ( byte i = 0; i < 6; i++ ) key.keyByte[i] = 0xFF;

  // Inicializar electroválvula
  pinMode( EV, OUTPUT );
  digitalWrite( EV, LOW );

  // Inicializar sensores de temperatura
  sensores.begin();

  // Inicializar caudalimetros
  for ( byte i = 0; i < CANTCERVEZAS; i++ )
    pinMode( CAUDA[i], INPUT );
  
  // Determinar los pines de los caudalimentros para que generen una interrupcion en el ciclo del Arduino.
  attachInterrupt( digitalPinToInterrupt( CAUDA[0] ), caudal1, RISING );
  attachInterrupt( digitalPinToInterrupt( CAUDA[1] ), caudal2, RISING );
  attachInterrupt( digitalPinToInterrupt( CAUDA[2] ), caudal3, RISING );
  attachInterrupt( digitalPinToInterrupt( CAUDA[3] ), caudal4, RISING );

  // Inicializar servo tarjeta
  pinMode( LED_VERDE, OUTPUT );
  pinMode( LED_ROJO, OUTPUT );
  digitalWrite( LED_VERDE, HIGH );
  digitalWrite( LED_ROJO, LOW );
  
  tarjeta.attach( PIN_SERVO_TARJETA ); // Indicar al objeto Servo "tarjeta" el pin donde esta conectado el servo.
  tarjeta.write( SERVO_ARRIBA );

  antes = millis();
}

void loop ()
{
  if ( conectado ) // Si ya se conecto al pc
  { 
    long ahora = millis();
    medirTemp -= ahora - antes;
    antes = ahora;
    
    // Mandar temperaturas
    if ( medirTemp <= 0 )
    {
      medirTemp = medirTempCada;
      
      sensores.requestTemperatures();
  
      Serial.print( "temp:" );
      Serial.println( sensores.getTempCByIndex( 0 ) ); // ByIndex, porque se puede tener mas de un sensor
    }
    
    // Chequear por tarjetas RFID
    if ( !rfid.PICC_IsNewCardPresent() )
      return; // Pasa si encuentra tarjeta nueva
  
    if ( !rfid.PICC_ReadCardSerial() )
      return; // Pasa si puede leer el serial de la tarjeta

    admin = "";
  
    // Resetear variables
    for ( byte q = 0; q < 4; q++ )
    {
      monto[q] = 0;
      pasosCaudal[q] = 0;
      totalCaudal[q] = 0;
    }
    puedeContar = false;
    
    leerBloque( B_ADMIN, lectura );
    admin = obtenerString( lectura, 16 );

    if ( admin == "admin" )
    {
      Serial.println( "tarjeta-admin" );
    }
    else
    {
      // Indica al pc que se ingresó una tarjeta
      Serial.print( "tarjeta:" );
      Serial.print( id( rfid.uid.uidByte, rfid.uid.size ) ); // Se obtiene el id
      Serial.println();
      tarjeta.write( SERVO_ABAJO );
      digitalWrite( LED_VERDE, LOW );
      digitalWrite( LED_ROJO, HIGH );
  
      leerBloque( BLOQUE, lectura ); // Se lee el bloque con el saldo
      nuevoSaldo = saldoInicial = obtenerString( lectura, 16 ).toFloat(); // Se pasa el bloque leido a String
      
      // Se pasa el saldo inicial al pc
      Serial.print( "saldoi:" );
      Serial.println( saldoInicial );

      puedeContar = false;
      digitalWrite( EV, saldoInicial > 0 ? HIGH : LOW ); // Se abren las electroválvulas si el saldo es mayor a 0
      // Este delay es para evitar que cuente cosas FALSAS!!!!!
      delay( 100 );
      puedeContar = saldoInicial > 0;
    }
    
    rfid.PCD_StopCrypto1(); // Magia...

    // Mientras se pueda acceder a la tarjeta
    while ( prepararTarjeta() )
    {
      if ( Serial.available() )
      {
        String s = Serial.readString();
        
        if ( s == "retirar" )
        {
          puedeContar = false;
          digitalWrite( EV, LOW );
          tarjeta.write( SERVO_ARRIBA );
          digitalWrite( LED_VERDE, HIGH );
          digitalWrite( LED_ROJO, LOW );
        }
      }
      
      bool pasoCaudal = false;
      
      for ( byte i = 0; i < CANTCERVEZAS; i++ ) // Se recorre cada una de las cervezas
      {
        if ( pasosCaudal[i] > 0 ) // Si uno de los caudales marco algo
        {
          totalCaudal[i] += pasosCaudal[i] / 0.450; // Se agrega al total del caudal
          monto[i] = totalCaudal[i] * precio[i]; // Se calcula el monto de ese total
          pasosCaudal[i] = 0; // Se marca que paso caudal

          // Se marca que paso caudal
          pasoCaudal = true;

          Serial.print( "cauda:" );
          Serial.print( ( i + 1 ) );
          Serial.print( ":" );
          Serial.println( totalCaudal[i] );
        }
      }

      if ( pasoCaudal )
      {
        float montoGastado = 0;
        
        // Se suma al monto total gastado
        for ( byte i = 0; i < CANTCERVEZAS; i++ )
        {
          // Se manda el monto al pc
          Serial.print( "monto:" );
          Serial.print( ( i + 1 ) );
          Serial.print( ":" );
          Serial.println( monto[i] );
          
          montoGastado += monto[i];
        }

        nuevoSaldo = saldoInicial - montoGastado; // Se calcula el nuevo saldo
        String( nuevoSaldo ).getBytes( contenido, 16 ); // Se guarda la variable en el arreglo contenido
        escribirBloque( BLOQUE, contenido ); // Se escribe el arreglo contenido en el bloque del saldo

        // Se comunica el nuevo saldo al pc
        Serial.print( "saldo:" );
        Serial.println( nuevoSaldo );

        rfid.PCD_StopCrypto1(); // Magia ...
        
        if ( nuevoSaldo <= 0 ) // Si el nuevo saldo es menor a 0, se cierran las electroválvulas
          digitalWrite( EV, LOW );
      }
    }
    
    // Cuando ya no se puede acceder a la tarjeta se avisa al pc
    Serial.println( "no-tarjeta" );

    // Resetear variables
    for ( byte q = 0; q < 4; q++ )
    {
      monto[q] = 0;
      pasosCaudal[q] = 0;
      totalCaudal[q] = 0;
    }
    puedeContar = false;

    // Al quitar la tarjeta se cierran las electroválvulas
    digitalWrite( EV, LOW );
  
    // Se desconecta internamente
    rfid.PICC_HaltA();
    rfid.PCD_StopCrypto1();
  }
  else
  {
    establecerConexion();
  }
}

// Convierte la id del buffer en un String
String id ( byte *buffer, byte bufferSize )
{
  String res = "";

  for ( byte i = 0; i < bufferSize; i++ )
    res += buffer[i];

  return res;
}
