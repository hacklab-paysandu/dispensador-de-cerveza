void establecerConexion ()
{
  if ( Serial.available() )
  {
    String info = Serial.readString();
  
    if ( info == "c" ) // Primer mensaje que va a llegar desde la pc, agregar \n para usar desde el monitor serial
    {
      recibido = true;
      Serial.println( "recibir" );
    }
    else // Si no es el primer mensaje, entonces hay que parsearlo
    {
      int sepIndex = info.indexOf( ':' ); // Índice del separador
      String dato = info.substring( 0, sepIndex ); // Dato a la izquierda del ':'
      String valor = info.substring( sepIndex + 1, info.length()); // Valor a la derecha del ':', restar -1 a info.length() para usar desde el monitor serial.

      // Guardar valores que van llegando
      if ( dato == "precio1" )
      {
        precio[0] = valor.toDouble() / 1000;
      }
      else if ( dato == "precio2" )
      {
        precio[1] = valor.toDouble() / 1000;
      }
      else if ( dato == "precio3" )
      {
        precio[2] = valor.toDouble() / 1000;
      }
      else if ( dato == "precio4" )
      {
        precio[3] = valor.toDouble() / 1000;
      }
      else if ( dato == "cantidad" )
      {
        CANTCERVEZAS = valor.toInt();
      }
      else if ( dato == "caudalAMl" )
      {
        caudalAMl = valor.toFloat();
      }
      else if ( dato == "medirTempCada" )
      {
        medirTempCada = valor.toInt();
      }

      // El último mensaje que se recibe es 'fin:fin'
      if ( dato == "fin" || valor == "fin" )
      {
        conectado = true;
      }
        
      // Enviar info recibida, para que la pc sepa que ya se procesó y se guardó
      Serial.println( info );
    }
  }
  
  if ( !recibido ) // Mientras no reciba el primer mensaje
  {
    Serial.println( "conectado" );
  }
}
